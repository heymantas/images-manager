<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


Route::get('/', 'DashboardController@index')->middleware('auth:sanctum');
Route::get('/images/search/status/{status}/image/{id}/project/{name}/month/{month}', 'DashboardController@searchImages')->middleware('auth:sanctum');
Route::get('/zips/search/status/{status}/zip/{id}/project/{name}/month/{month}', 'DashboardController@searchZips')->middleware('auth:sanctum');

Route::get('/image/view/{id}', 'ImageController@single')->middleware('auth:sanctum')->name('image.view');
Route::get('/zip/view/{id}', 'ZipController@single')->middleware('auth:sanctum')->name('zip.view');

Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/about-bg', function () {
        return Inertia::render('BgRemoval/AboutBgRemoval');
    })->name('about-bg');

    Route::get('/about-editing', function () {
        return Inertia::render('Images/AboutImageEditing');
    })->name('about-editing');
});

Route::get('/contact-us', 'ContactController@index')
    ->middleware('auth:sanctum')
    ->name('contact.us');

Route::post('/contact', 'ContactController@store')
    ->middleware('auth:sanctum');


Route::delete('/deleteMyPhoto/{id}', 'UserController@deleteMyProfilePicture')->middleware('auth:sanctum');
Route::get('/user/profile-photo/{id}', 'UserController@showProfilePhoto')->middleware('auth:sanctum');

Route::group(['middleware' => ['auth:sanctum', 'verified', 'role:Client']], function () {

    //images
    Route::get('/images', "ImageController@getImagesByClient")->name('clientImages.index');
    Route::post('/savePhoto', "ImageController@store")->name('clientImages.store');
    Route::get('/client/image/{id}', "ImageController@showClientImage");
    Route::get('/client/edited-image/{id}', "ImageController@showEditedImage");
    Route::delete('/image/delete/{id}', "ImageController@deleteImage");

    //zips
    Route::get('/upload-zip', 'ZipController@index')->name('zip.index');
    Route::post('/upload-zip', 'ZipController@uploadZip')->name('zip.upload');
    Route::get('/client/zip/{id}', "ZipController@showZip");
    Route::get('/client/edited-zip/{id}', "ZipController@showEditedZip");
    Route::delete('/zip/delete/{id}', "ZipController@deleteZip");

    //bg removal
    Route::get('/bg-removal', 'ImageController@bgRemoval')->name('clientImages.bg-removal');
    Route::post('/removeBackground', 'ImageController@removeBackground');
});

Route::group(['middleware' => ['auth:sanctum', 'verified', 'role:Editor']], function () {

    //images
    Route::get('/editor-images', "ImageController@getAllImages")->name('allImages.index');
    Route::get('/image/edit/{id}', "ImageController@edit")->name('image.edit');
    Route::put('/updateClientImage', "ImageController@update")->name('image.update');
    Route::get('/editor/image/{id}', "ImageController@showImage");
    Route::get('/editor/edited-image/{id}', "ImageController@showEditedImage");
    Route::delete('/editor/image/delete/{id}', "ImageController@deleteImage");

    //zips
    Route::get('/editor-zips', "ZipController@getAllZips")->name('allZips.index');
    Route::get('/zip/edit/{id}', "ZipController@edit")->name('zip.edit');
    Route::put('/updateClientZip', "ZipController@updateZip")->name('zip.update');
    Route::delete('/editor/zip/delete/{id}', "ZipController@deleteZip");
    Route::get('/editor/zip/{id}', "ZipController@showZip");
    Route::get('/editor/edited-zip/{id}', "ZipController@showEditedZip");


});

//Admin routes
Route::group(['middleware' => ['auth:sanctum', 'verified', 'role:Admin']], function () {
    Route::get('/users', "UserController@index")->name('user.index');
    Route::get('/user/create', "UserController@create")->name('user.create');
    Route::post('/createUser', "UserController@store")->name('user.store');
    Route::get('/user/edit/{id}', "UserController@edit")->name('user.edit');
    Route::put('/updateUser', 'UserController@update')->name('user.update');
    Route::delete('/deletePhoto/{id}', 'UserController@deleteProfilePicture');
    Route::delete('/admin/deleteUser/{id}', 'UserController@deleteUser');


    Route::get('/admin/image/{id}', "ImageController@showImage");
    Route::get('/admin/edited-image/{id}', "ImageController@showEditedImage");

    Route::get('/admin/zip/{id}', "ZipController@showZip");
    Route::get('/admin/edited-zip/{id}', "ZipController@showEditedZip");
});
