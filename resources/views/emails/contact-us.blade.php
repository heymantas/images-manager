@component('mail::message')
# New message from - {{$name}}
<p>{{$message}}</p>
@endcomponent
