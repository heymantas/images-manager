@component('mail::message')
# New image uploaded!

Dear {{$editor->name}},<br/>
The EME Clipping Path site has just received a new image from {{$client->name}}.

Image details: <br/>
Height: {{$image->height}} px <br/>
Width: {{$image->width}} px <br/>
File size: {{round($image->size / 1000, 2)}} Kb <br/>
Project name: {{$image->project_name}} <br/>
Image brief: {{$image->note}} <br/>

Thanks,<br>
The ClippingPath Team
@endcomponent
