@component('mail::message')
@if($receiverRole[0]->role_id == 1)
# A image was edited

Dear {{$user->name}},<br/>
The image has been edited and is ready for download on clipping.emedigital.co.uk web portal

Edited image details: <br/>
Height: {{$image->height}} px <br/>
Width: {{$image->width}} px <br/>
File size: {{round($image->size / 1000, 2)}} Kb <br/>
Edited image brief: {{$image->note}} <br/>

Regards,<br>
Account Admin Team
@else
# Your image was edited!

Dear {{$client->name}},<br/>
The image has been edited and is ready for download on clipping.emedigital.co.uk web portal

Edited image details: <br/>
Height: {{$image->height}} px <br/>
Width: {{$image->width}} px <br/>
File size: {{round($image->size / 1000, 2)}} Kb <br/>
Edited image brief: {{$image->note}} <br/>

Regards,<br>
Account Admin Team
@endif
@endcomponent
