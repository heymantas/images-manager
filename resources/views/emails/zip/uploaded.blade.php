@component('mail::message')
# New zip uploaded!

Dear {{$editor->name}},<br/>
The EME Clipping Path site has just received a new zip from {{$client->name}} .

Zip details: <br/>
File size: {{round($zip->size / 1000, 2)}} Kb <br/>
Project name: {{$zip->project_name}} <br/>
Zip brief: {{$zip->note}} <br/>

Thanks,<br>
The ClippingPath Team
@endcomponent
