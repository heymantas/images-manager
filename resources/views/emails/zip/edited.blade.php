@component('mail::message')

@if($receiverRole[0]->role_id == 1)
# A zip was edited!

Dear {{$user->name}},<br/>
The zip has been edited and is ready for download on clipping.emedigital.co.uk web portal

Edited zip details: <br/>
File size: {{round($zip->size / 1000, 2)}} Kb <br/>
Edited zip brief: {{$zip->note}} <br/>

Regards,<br>
Account Admin Team
@else
# Your zip was edited!

Dear {{$client->name}},<br/>
The zip has been edited and is ready for download on clipping.emedigital.co.uk web portal

Edited zip details: <br/>
File size: {{round($zip->size / 1000, 2)}} Kb <br/>
Edited zip brief: {{$zip->note}} <br/>

Regards,<br>
Account Admin Team
@endif
@endcomponent
