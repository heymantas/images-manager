<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => "Administrator",
            'email' => "admin@test.lt",
            'email_verified_at' => now(),
            'password' => bcrypt('secret'),
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => "Editor",
            'email' => "editor@test.lt",
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ]);

        //More editors

        $faker = Faker::create();
        for($i = 3; $i<=10; $i++){
            DB::table('users')->insert([
                'id' => $i,
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail(),
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ]);
        }

        DB::table('users')->insert([
            'id' => 11,
            'name' => "Client",
            'email' => "client@test.lt",
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
        ]);

        //More clients

        $faker = Faker::create();
        for($i = 12; $i <= 20; $i++){
            DB::table('users')->insert([
                'id' => $i,
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail(),
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
            ]);
        }

    }
}
