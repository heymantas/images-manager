<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->insert([
            [
                'user_id' => '1',
                'role_id' => '1'
            ],
            [
                'user_id' => '2',
                'role_id' => '2'
            ],
        ]);


        for($i = 3; $i<=10; $i++){
            DB::table('role_user')->insert([
                [
                    'user_id' => $i,
                    'role_id' => '2'
                ]
            ]);
        }

        DB::table('role_user')->insert([
            [
                'user_id' => '11',
                'role_id' => '3'
            ]
        ]);

        for($i = 12; $i<=20; $i++){
            DB::table('role_user')->insert([
                [
                    'user_id' => $i,
                    'role_id' => '3'
                ]
            ]);
        }


    }
}
