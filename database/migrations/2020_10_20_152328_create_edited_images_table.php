<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEditedImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edited_images', function (Blueprint $table) {
            $table->id();
            $table->text('filename');
            $table->text('url');
            $table->double('height');
            $table->double('width');
            $table->double('size');
            $table->string("note")->nullable();
            $table->bigInteger('editor_id')->unsigned();
            $table->bigInteger('uploaded_image_id')->unsigned();
            $table->foreign('editor_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('uploaded_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edited_images');
    }
}
