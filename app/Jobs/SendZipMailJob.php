<?php

namespace App\Jobs;

use App\Mail\ZipEdited;
use App\Mail\ZipUploaded;
use App\Models\RoleUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendZipMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $zip;
    protected $client;
    protected $isEdited;

    public function __construct($user, $zip, $client, $isEdited)
    {
        $this->user = $user;
        $this->zip = $zip;
        $this->client = $client;
        $this->isEdited = $isEdited;
    }

    public function handle()
    {
        if($this->isEdited) {

            $role = RoleUser::where('user_id', $this->user->id)->get();

            if($role[0]->role_id == 1) {
                Mail::to($this->user->email)
                    ->queue(new ZipEdited(
                        $this->user,
                        $this->zip,
                        $this->client
                    ));
            }
            else {
                Mail::to($this->client->email)
                    ->queue(new ZipEdited(
                        $this->user,
                        $this->zip,
                        $this->client
                    ));
            }
        } else {
            Mail::to($this->user->email)
                ->queue(new ZipUploaded(
                    $this->user,
                    $this->zip,
                    $this->client
                ));
        }
    }
}
