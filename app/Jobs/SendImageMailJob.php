<?php

namespace App\Jobs;

use App\Mail\ImageEdited;
use App\Mail\ImageUploaded;
use App\Models\RoleUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendImageMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $image;
    protected $client;
    protected $isEdited;

    public function __construct($user, $image, $client, $isEdited)
    {
        $this->user = $user;
        $this->image = $image;
        $this->client = $client;
        $this->isEdited = $isEdited;
    }

    public function handle()
    {
        if($this->isEdited) {

            $role = RoleUser::where('user_id', $this->user->id)->get();

            if($role[0]->role_id == 1) {
                Mail::to($this->user->email)
                    ->send(new ImageEdited(
                        $this->user,
                        $this->image,
                        $this->client
                    ));
            } else {
                Mail::to($this->client->email)
                    ->send(new ImageEdited(
                        $this->user,
                        $this->image,
                        $this->client
                    ));
            }
        } else {
            Mail::to($this->user->email)
                ->send(new ImageUploaded(
                    $this->user,
                    $this->image,
                    $this->client
                ));
        }
    }
}
