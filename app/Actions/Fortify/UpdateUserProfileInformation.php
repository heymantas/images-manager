<?php

namespace App\Actions\Fortify;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function update($user, array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
        ])->validateWithBag('updateProfileInformation');

        if (isset($input['photo'])) {
//            $user->updateProfilePhoto($input['photo']);

            $fileExists = Storage::disk('local')->exists('/profile-photos/' . $user->profile_photo_path);
            if($fileExists) {
                Storage::disk('local')->delete('/profile-photos/' . $user->profile_photo_path);
            }

            $image = base64_encode(file_get_contents($input['photo']));
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = Str::random(10) . '.png';
            Storage::disk('local')->put("./profile-photos/".$imageName, base64_decode($image));
            $user->profile_photo_path = $imageName;
        }

        if ($input['email'] !== $user->email &
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            $user->forceFill([
                'name' => $input['name'],
                'email' => $input['email'],
                'position' => $input['position'],
                'company' => $input['company'],
                'address' => $input['address'],
                'post_code' => $input['post_code'],
                'country' => $input['country'],
                'telephone' => $input['telephone'],
                'fax' => $input['fax'],
            ])->save();
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        $user->forceFill([
            'name' => $input['name'],
            'email' => $input['email'],
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }
}
