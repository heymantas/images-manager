<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ImageUploaded extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $editor;
    public $image;
    public $client;


    public function __construct($editor, $image, $client)
    {
        $this->editor = $editor;
        $this->image = $image;
        $this->client = $client;
    }


    public function build()
    {
        return $this->from($this->client->email)
            ->subject("New image uploaded by ". $this->client->name)
            ->markdown('emails.image.uploaded');
    }
}
