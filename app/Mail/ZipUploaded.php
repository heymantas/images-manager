<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ZipUploaded extends Mailable
{
    use Queueable, SerializesModels;

    public $editor;
    public $zip;
    public $client;


    public function __construct($editor, $zip, $client)
    {
        $this->editor = $editor;
        $this->zip = $zip;
        $this->client = $client;
    }


    public function build()
    {
        return $this->from($this->client->email)
            ->subject("New zip uploaded by ". $this->client->name)
            ->markdown('emails.zip.uploaded');
    }
}
