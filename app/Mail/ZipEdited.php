<?php

namespace App\Mail;

use App\Models\RoleUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ZipEdited extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $zip;
    public $client;
    public $receiverRole;

    public function __construct($user, $zip, $client)
    {
        $this->user = $user;
        $this->zip = $zip;
        $this->client = $client;
        $this->receiverRole = RoleUser::where('user_id', $user->id)->get();
    }

    public function build()
    {
        if($this->receiverRole[0]->role_id == 1) {
            return $this->from($this->user->email)
                ->subject("A ZIP was edited in EMI Digital site!")
                ->markdown('emails.zip.edited');
        } else {
            return $this->from($this->user->email)
                ->subject("Your zip was edited by ". $this->user->name)
                ->markdown('emails.zip.edited');
        }
    }
}
