<?php

namespace App\Mail;

use App\Models\RoleUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ImageEdited extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $image;
    public $client;
    public $receiverRole;

    public function __construct($user, $image, $client)
    {
        $this->user = $user;
        $this->image = $image;
        $this->client = $client;
        $this->receiverRole = RoleUser::where('user_id', $user->id)->get();
    }

    public function build()
    {
        if($this->receiverRole[0]->role_id == 1) {
            return $this->from($this->user->email)
                ->subject("A image was edited in EMI Digital site!")
                ->markdown('emails.image.edited');
        } else {
            return $this->from($this->user->email)
                ->subject("Your image was edited by ". $this->user->name)
                ->markdown('emails.image.edited');
        }
    }
}
