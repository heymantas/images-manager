<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zip extends Model
{
    use HasFactory;

    protected $table = "zips";

    public function client() {
        return $this->belongsTo(User::class);
    }

    public function editedZip() {
        return $this->hasOne(EditedZip::class, 'uploaded_zip_id')->with('editor');
    }
}
