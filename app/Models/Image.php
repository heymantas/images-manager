<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $table = 'images';

    protected $fillable = [
        'id',
        'filename',
        'url',
        'height',
        'width',
        'size'
    ];

    public function client() {
        return $this->belongsTo(User::class);
    }

    public function editedImage() {
        return $this->hasOne(EditedImage::class, 'uploaded_image_id')->with('editor');
    }
}
