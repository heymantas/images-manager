<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditedImage extends Model
{
    use HasFactory;

    protected $table = 'edited_images';

    public function uploadedImage() {
        return $this->belongsTo(Image::class)->with('client');
    }

    public function editor() {
        return $this->belongsTo(User::class);
    }
}
