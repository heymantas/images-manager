<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditedZip extends Model
{
    use HasFactory;

    protected $table = "edited_zips";

    public function uploadedZip() {
        return $this->belongsTo(Zip::class)->with('client');
    }

    public function editor() {
        return $this->belongsTo(User::class);
    }
}
