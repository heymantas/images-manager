<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param array $roles
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ... $roles)
    {
        if(!$request->user()->authorizeRoles($roles)){
            abort(401,'Unauthorized');
        }
        return $next($request);
    }
}
