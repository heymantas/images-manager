<?php

namespace App\Http\Controllers;

use App\Models\EditedImage;
use App\Models\Image;
use App\Models\RoleUser;
use App\Models\Zip;
use Carbon\Carbon;
use Database\Seeders\ImagesSeeder;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index() {

        $userId = Auth::user()->id;
        $role = RoleUser::where('user_id', $userId)->get();

        return Inertia::render('Dashboard/Dashboard', [
            'role' => $role[0],
        ]);
    }

    public function searchImages($status, $id, $name, $month) {

        $status = intval($status);

        $images = Image::with('editedImage')
            ->orderBy('created_at', 'desc')
            ->when($status < 2, function ($query) use ($status) {
                return $query->where('is_edited', $status);
            })
            ->when($id != "null", function ($query) use ($id) {
                return $query->where('id', 'LIKE', '%'.$id.'%');
            })
            ->when($name != "null", function ($query) use ($name) {
                return $query->where('project_name', 'LIKE', '%'.$name.'%');
            })
            ->when($month == "this", function ($query) use ($month) {
                return $query->whereMonth('created_at', now()->month)->whereYear('created_at', now()->year);
            })
            ->when($month == "last", function ($query) use ($month) {
                return $query->whereMonth('created_at', now()->subMonth())->whereYear('created_at', now()->year);
            })
            ->when($month == "last12", function ($query) use ($month) {
                return $query->whereDate('created_at', ">", now()->subYear());
            })
            ->with('client')
            ->whereHas('client', function ($query) {
                $userId = Auth::user()->id;
                $role = RoleUser::where('user_id', $userId)->get();
                if($role[0]->role_id == 3) { //client
                    $query->where('id', $userId);
                }
            })
            ->paginate(10);


        return response()->json($images);
    }

    public function searchZips($status, $id, $name, $month) {

        $status = intval($status);

        $zips = Zip::with('editedZip')
            ->orderBy('created_at', 'desc')
            ->when($status < 2, function ($query) use ($status) {
                return $query->where('is_edited', $status);
            })
            ->when($id != "null", function ($query) use ($id) {
                return $query->where('id', 'LIKE', '%'.$id.'%');
            })
            ->when($name != "null", function ($query) use ($name) {
                return $query->where('project_name', 'LIKE', '%'.$name.'%');
            })
            ->when($month == "this", function ($query) use ($month) {
                return $query->whereMonth('created_at', now()->month)->whereYear('created_at', now()->year);
            })
            ->when($month == "last", function ($query) use ($month) {
                return $query->whereMonth('created_at', now()->subMonth())->whereYear('created_at', now()->year);
            })
            ->when($month == "last12", function ($query) use ($month) {
                return $query->whereDate('created_at', ">", now()->subYear());
            })
            ->with('client')->paginate(10);


        return response()->json($zips);
    }
}
