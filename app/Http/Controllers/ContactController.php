<?php

namespace App\Http\Controllers;
use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;

class ContactController extends Controller
{
    public function index() {
        return Inertia::render('Contact/Contact');
    }

    public function store(Request $request) {
        $request->validate([
            'name' => "required",
            'email' => "required|email",
            'subject' => "required",
            'message' => "required",
        ]);

        Mail::to('admin@test.lt')
            ->queue(new ContactUs(
                $request->name,
                $request->email,
                $request->subject,
                $request->message
            ));

        return redirect()->route('dashboard');
    }
}
