<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Image;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;

class UserController extends Controller
{

    public function index()
    {
        $clients = User::whereHas('roles', function ($q) {
            $q->where('id', '=', 3);
        })->with('roles')->get();

        $editors = User::whereHas('roles', function ($q) {
            $q->where('id', '=', 2);
        })->with('roles')->get();

        return Inertia::render('Users/Index', [
            'clients' => $clients,
            'editors' => $editors
        ]);

    }

    public function create()
    {
        return Inertia::render('Users/UserCreate');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => "required",
            'email' => "required|email|unique:users",
            'password' => "required|min:5",
            'role_id' => "required|numeric|min:2|max:3",
        ]);

        $user = new User();
        $user->email_verified_at = now();
        $user->name = $request->name;
        $user->position = $request->position;
        $user->company = $request->company;
        $user->address = $request->address;
        $user->post_code = $request->post_code;
        $user->country = $request->country;
        $user->telephone = $request->telephone;
        $user->fax = $request->fax;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        RoleUser::create([
            'user_id' => $user->id,
            'role_id' => $request->role_id,
        ]);

        return redirect()->route('user.index');
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->with('roles')->get();
        if($user[0]->roles[0]->id == 1) {
            return redirect()->route('user.index');
        } else {
            return Inertia::render('Users/UserEdit', [
                'editUser' => $user
            ]);
        }
    }

    public function deleteUser($id) {
        $user = User::where('id', $id)->first();

        if($user->profile_photo_path) {
            $fileExists = Storage::disk('local')->exists('/profile-photos/' . $user->profile_photo_path);
            if($fileExists) {
                Storage::disk('local')->delete('/profile-photos/' . $user->profile_photo_path);
            }
        }

        if($user) {
            User::where('id', $id)->delete();
        }

        return redirect()->route('user.index');
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => "required",
            'name' => "required",
            'email' => "required|email|unique:users,email,".$request->id,
            'role_id' => "required|numeric|min:2|max:3",
        ]);

        $user = User::findOrFail($request->id);

        if($request->get('photo')) {

            $fileExists = Storage::disk('local')->exists('/profile-photos/' . $user->profile_photo_path);
            if($fileExists) {
                Storage::disk('local')->delete('/profile-photos/' . $user->profile_photo_path);
            }

            $image = base64_encode(file_get_contents($request->get('photo')));
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = Str::random(10) . '.png';
            Storage::disk('local')->put("./profile-photos/".$imageName, base64_decode($image));
            $user->profile_photo_path = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->position = $request->position;
        $user->company = $request->company;
        $user->address = $request->address;
        $user->post_code = $request->post_code;
        $user->country = $request->country;
        $user->telephone = $request->telephone;
        $user->fax = $request->fax;
        $user->save();

        RoleUser::where('user_id', $request->id)->update([
            'role_id' => $request->role_id,
        ]);
    }


    public function destroy(Client $client)
    {
        //
    }

    public function deleteProfilePicture($id) {

        $user = User::findOrFail($id);

        $fileExists = Storage::disk('local')->exists('/profile-photos/' . $user->profile_photo_path);
        if($fileExists) {
            Storage::disk('local')->delete('/profile-photos/' . $user->profile_photo_path);

            $user->profile_photo_path = null;
            $user->save();

            return redirect()->route('user.index');
        }
    }

    public function deleteMyProfilePicture($id) {

        $user = User::findOrFail($id);

        $fileExists = Storage::disk('local')->exists('/profile-photos/' . $user->profile_photo_path);
        if($fileExists) {
            Storage::disk('local')->delete('/profile-photos/' . $user->profile_photo_path);

            $user->profile_photo_path = null;
            $user->save();

            return redirect()->route('profile.show');
        }
    }

    public function getUserRole() {
        $userId = Auth::user()->id;
        $role = RoleUser::where('user_id', $userId)->get();

        return response()->json($role);
    }

    public function showProfilePhoto($id) {
        $user = User::where([
            'id' => $id,
        ])->first();
        $responseImage = Storage::disk('local')->response('profile-photos/' . $user->profile_photo_path);
        if($user && $responseImage) {
            return $responseImage;
        } else {
            return redirect()->route('dashboard');
        }
    }
}
