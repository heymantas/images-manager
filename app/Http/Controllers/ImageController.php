<?php

namespace App\Http\Controllers;

use App\Jobs\SendImageMailJob;
use App\Mail\ImageEdited;
use App\Mail\ImageUploaded;
use App\Models\EditedImage;
use App\Models\Image;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Zip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;

class ImageController extends Controller
{
    public function getImagesByClient()
    {
        $userId = Auth::user()->id;
        $images = Image::where([
            'client_id' => $userId,
            'is_edited' => false
        ])->with('client')->get();
        $editedImages = EditedImage::with('uploadedImage')->with('editor')->get();
        return Inertia::render('Images/ClientImages', [
            'images' => $images,
            'editedImages' => $editedImages
        ]);
    }

    public function bgRemoval() {
        return Inertia::render('BgRemoval/BgRemoval');
    }

    public function removeBackground(Request $request) {
        $res = Http::withHeaders([
            'X-Api-Key' => env('REMOVE_BG_KEY', ''),
        ])
            ->post('https://api.remove.bg/v1.0/removebg', [
                'image_file_b64' => $request->image
            ]);

        if($res->successful()) {
            $width = getimagesize('data:image/png;base64,' . base64_encode($res))[0];
            $height = getimagesize('data:image/png;base64,' . base64_encode($res))[1];
            $base64 = 'data:image/png;base64,' . base64_encode($res);
            $fileSize = strlen(base64_decode($res));

            return [
                'imageWithoutBackground' => [
                    'width' => $width,
                    'height' => $height,
                    'fileSize' => $fileSize,
                    'base64' => $base64
                ]
            ];
        } else if($res->failed()) {
            return $res['errors'];
        } else if($res->serverError()) {
            return $res->throw();
        }
    }

    public function getAllImages()
    {
        $userId = Auth::user()->id;
        $images = Image::with('client')->where('is_edited', false)->get();
        $editedImages = EditedImage::with('uploadedImage')
            ->where('editor_id', $userId)
            ->with('editor')
            ->get();
        return Inertia::render('Images/EditorImages', [
            'images' => $images,
            'editedImages' => $editedImages
        ]);
    }

    public function edit($id)
    {
        $image = Image::where([
            'id' => $id,
            'is_edited' => false
        ])->with('client')->get();

        if(sizeof($image) > 0) {
            return Inertia::render('Images/EditImage', [
                'editImage' => $image
            ]);
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'project_name' => "required",
            'images' => "required",
            'client_id' => "required|numeric"
        ]);

        $pos  = strpos($request->images[0]['photoPreview'], ';');
        $type = explode(':', substr($request->images[0]['photoPreview'], 0, $pos))[1];

        $editors = User::whereHas('roles', function ($q) {
            $q->where('id', '=', 2);
        })->with('roles')->get();

        $admins = User::whereHas('roles', function ($q) {
            $q->where('id', '=', 1);
        })->with('roles')->get();

        $client = User::where('id', $request->client_id)->first();

        foreach ($request->images as $selectedImage) {
            $image = new Image(); //model, not actual image

            $pos  = strpos($selectedImage['photoPreview'], ';');
            $type = explode(':', substr($selectedImage['photoPreview'], 0, $pos))[1];

            $path = null;
            $imageName = "";

            if($type == "image/png") {
                $imageToUpload = base64_encode(file_get_contents($selectedImage['photoPreview']));
                $imageToUpload = str_replace('data:image/png;base64,', '', $imageToUpload);
                $imageToUpload = str_replace(' ', '+', $imageToUpload);
                $imageName = Str::random(10) . '.png';
                $path = Storage::disk('local')->put("./images/" . $imageName, base64_decode($imageToUpload));
            } else if($type == "image/jpeg") {
                $imageToUpload = base64_encode(file_get_contents($selectedImage['photoPreview']));
                $imageToUpload = str_replace('data:image/jpeg;base64,', '', $imageToUpload);
                $imageToUpload = str_replace(' ', '+', $imageToUpload);
                $imageName = Str::random(10) . '.jpeg';
                $path = Storage::disk('local')->put("./images/" . $imageName, base64_decode($imageToUpload));
            }

            $image->filename = $imageName;
            $image->url = Storage::disk('local')->url($path);
            $image->height = $selectedImage['height'];
            $image->width = $selectedImage['width'];
            $image->size = $selectedImage['size'];
            $image->note = $request->note;
            $image->project_name = $request->project_name;
            $image->client_id = $request->client_id;
            $image->save();

            foreach($editors as $editor) {
                SendImageMailJob::dispatch($editor, $image, $client, false);
            }

            foreach($admins as $admin) {
                SendImageMailJob::dispatch($admin, $image, $client, false);
            }
        }

        return redirect()->route('dashboard');
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => "required",
            'image' => "required",
            'height' => "required",
            'width' => "required",
            'size' => "required",
            'editor_id' => "required",
            'client_id' => "required"
        ]);

        $image = Image::findOrFail($request->id);
        $image->is_edited = true;
        $image->save();

        $editedImage = new EditedImage();

        $pos  = strpos($request->get('image'), ';');
        $type = explode(':', substr($request->get('image'), 0, $pos))[1];
        $path = null;
        $imageName = "";

        if($type == "image/png") {
            $imageToUpload = base64_encode(file_get_contents($request->get('image')));
            $imageToUpload = str_replace('data:image/png;base64,', '', $imageToUpload);
            $imageToUpload = str_replace(' ', '+', $imageToUpload);
            $imageName = Str::random(10) . '.png';
            $path = Storage::disk('local')->put("./edited-images/" . $imageName, base64_decode($imageToUpload));
        } else if($type == "image/jpeg") {
            $imageToUpload = base64_encode(file_get_contents($request->get('image')));
            $imageToUpload = str_replace('data:image/jpeg;base64,', '', $imageToUpload);
            $imageToUpload = str_replace(' ', '+', $imageToUpload);
            $imageName = Str::random(10) . '.jpeg';
            $path = Storage::disk('local')->put("./edited-images/" . $imageName, base64_decode($imageToUpload));
        }

        $editedImage->filename = $imageName;
        $editedImage->url = Storage::disk('local')->url($path);
        $editedImage->height = $request->height;
        $editedImage->width = $request->width;
        $editedImage->size = $request->size;
        $editedImage->editor_id = $request->editor_id;
        $editedImage->note = $request->note;
        $editedImage->uploaded_image_id = $request->id;
        $editedImage->save();

        $editor = User::where('id', $request->editor_id)->first();
        $client = User::where('id', $request->client_id)->first();
        SendImageMailJob::dispatch($editor, $editedImage, $client, true);

        $admins = User::whereHas('roles', function ($q) {
            $q->where('id', '=', 1);
        })->with('roles')->get();

        foreach ($admins as $admin) {
            SendImageMailJob::dispatch($admin, $editedImage, $client, true);
        }

        return redirect()->route('dashboard');
    }

    public function showClientImage($id)
    {
        $userId = Auth::user()->id;
        $image = Image::where([
            'id' => $id,
            'client_id' => $userId
        ])->first();
        if ($image) {
            return Storage::disk('local')->response('images/' . $image->filename);
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function showImage($id)
    {
        $image = Image::where('id', $id)->first();
        if ($image) {
            return Storage::disk('local')->response('images/' . $image->filename);
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function showEditedImage($id)
    {
        $image = EditedImage::where('id', $id)->first();
        if ($image) {
            return Storage::disk('local')->response('edited-images/' . $image->filename);
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function deleteImage($id) {

        $image = Image::where([
            'id' => $id,
            'is_edited' => false
        ])->first();

        $fileExists = Storage::disk('local')->exists('/images/' . $image->filename);
        if($fileExists && $image) {
            Storage::disk('local')->delete('/images/' . $image->filename);
            $image->delete();
        }

        $userId = Auth::user()->id;
        $role = RoleUser::where('user_id', $userId)->get();

        if($role[0]->role_id == 3) {
            return redirect()->route('dashboard');
        } else if($role[0]->role_id == 2) {
            return redirect()->route('dashboard');
        }
    }

    public function single($id) {
        $image = Image::where('id', $id)->with('editedImage')->with('client')->first();

        return Inertia::render('Images/SingleImage', [
            'image' => $image
        ]);
    }
}
