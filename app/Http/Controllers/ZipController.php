<?php

namespace App\Http\Controllers;

use App\Jobs\SendZipMailJob;
use App\Models\EditedZip;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Zip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;

class ZipController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->id;
        $zips = Zip::where([
            'client_id' => $userId,
            'is_edited' => false
        ])->with('client')->get();
        $editedZips = EditedZip::with('uploadedZip')->with('editor')->get();
        return Inertia::render('ZipUpload/ZipUpload', [
            'zips' => $zips,
            'editedZips' => $editedZips
        ]);
    }

    public function getAllZips() {

        $userId = Auth::user()->id;
        $zips = Zip::where([
            'is_edited' => false
        ])->with('client')->get();

        $editedZips = EditedZip::with('uploadedZip')
            ->where('editor_id', $userId)
            ->with('editor')
            ->get();
        return Inertia::render('ZipUpload/EditorZips', [
            'editorZips' => $zips,
            'editedZips' => $editedZips
        ]);
    }

    public function edit($id) {
        $zip = Zip::where([
            'id' => $id,
            'is_edited' => false
        ])->with('client')->first();
        if($zip) {
            return Inertia::render('ZipUpload/EditZip', [
                'editZip' => $zip
            ]);
        } else {
            return redirect()->route('dashboard');
        }
    }

    public function uploadZip(Request $request) {
        $request->validate([
            'file' => "required",
            'size' => "required|numeric",
            'client_id' => "required|numeric"
        ]);

        $zip = new Zip();

        $pos  = strpos($request->file, ';');
        $type = explode(':', substr($request->file, 0, $pos))[1];
        $path = null;

        if($type == "application/x-zip-compressed") {
            $zipToUpload = $imageToUpload = base64_encode(file_get_contents($request->file));
            $zipToUpload = str_replace('data:application/x-zip-compressed;base64,', '', $zipToUpload);
            $zipToUpload = str_replace(' ', '+', $zipToUpload);
            $zipName = Str::random(10) . '.zip';
            $path = Storage::disk('local')->put('./zips/' . $zipName, base64_decode($zipToUpload));

            $zip->filename = $zipName;
            $zip->url = Storage::disk('local')->url($path);
            $zip->size = $request->size;
            $zip->note = $request->note;
            $zip->project_name = $request->project_name;
            $zip->client_id = $request->client_id;
            $zip->save();

            $editors = User::whereHas('roles', function ($q) {
                $q->where('id', '=', 2);
            })->with('roles')->get();

            $client = User::where('id', $request->client_id)->first();

            $admins = User::whereHas('roles', function ($q) {
                $q->where('id', '=', 1);
            })->with('roles')->get();

            foreach($editors as $editor) {
                SendZipMailJob::dispatch($editor, $zip, $client, false);
            }

            foreach($admins as $admin) {
                SendZipMailJob::dispatch($admin, $zip, $client, false);
            }
        }

        return redirect()->route('dashboard');
    }

    public function updateZip(Request $request) {
        $request->validate([
            'id' => "required",
            'file' => "required",
            'size' => "required",
            'editor_id' => "required",
            'client_id' => "required"
        ]);

        $pos  = strpos($request->file, ';');
        $type = explode(':', substr($request->file, 0, $pos))[1];
        $path = null;

        if($type == "application/x-zip-compressed") {
            $image = Zip::findOrFail($request->id);
            $image->is_edited = true;
            $image->save();

            $editedZip = new EditedZip();

            $zipToUpload = base64_encode(file_get_contents($request->file));
            $zipToUpload = str_replace('data:application/x-zip-compressed;base64,', '', $zipToUpload);
            $zipToUpload = str_replace(' ', '+', $zipToUpload);
            $zipName = Str::random(10) . '.zip';
            $path = Storage::disk('local')->put("./edited-zips/" . $zipName, base64_decode($zipToUpload));

            $editedZip->filename = $zipName;
            $editedZip->url = Storage::disk('local')->url($path);
            $editedZip->size = $request->size;
            $editedZip->note = $request->note;
            $editedZip->editor_id = $request->editor_id;
            $editedZip->uploaded_zip_id = $request->id;
            $editedZip->save();

            $editor = User::where('id', $request->editor_id)->first();
            $client = User::where('id', $request->client_id)->first();

            SendZipMailJob::dispatch($editor, $editedZip, $client, true);

            $admins = User::whereHas('roles', function ($q) {
                $q->where('id', '=', 1);
            })->with('roles')->get();

            foreach($admins as $admin) {
                SendZipMailJob::dispatch($admin, $editedZip, $client, true);
            }
        }

        return redirect()->route('dashboard');

    }

    public function deleteZip($id) {

        $zip = Zip::where([
            'id' => $id,
            'is_edited' => false
        ])->first();

        $fileExists = Storage::disk('local')->exists('/zips/' . $zip->filename);
        if($fileExists && $zip) {
            Storage::disk('local')->delete('/zips/' . $zip->filename);
            $zip->delete();
        }

        $userId = Auth::user()->id;
        $role = RoleUser::where('user_id', $userId)->get();

        if($role[0]->role_id == 3) {
            return redirect()->route('dashboard');
        } else if($role[0]->role_id == 2) {
            return redirect()->route('dashboard');
        }
    }

    public function showZip($id)
    {
        $zip = Zip::where('id', $id)->first();
        if ($zip) {
            return Storage::disk('local')->response('zips/' . $zip->filename);
        } else {

            $userId = Auth::user()->id;
            $role = RoleUser::where('user_id', $userId)->get();

            if($role[0]->role_id == 3) {
                return redirect()->route('dashboard');
            } else if($role[0]->role_id == 2) {
                return redirect()->route('dashboard');
            }
        }
    }

    public function showEditedZip($id)
    {
        $zip = EditedZip::where('id', $id)->first();
        if ($zip) {
            return Storage::disk('local')->response('edited-zips/' . $zip->filename);
        } else {

            $userId = Auth::user()->id;
            $role = RoleUser::where('user_id', $userId)->get();

            if($role[0]->role_id == 3) {
                return redirect()->route('dashboard');
            } else if($role[0]->role_id == 2) {
                return redirect()->route('dashboard');
            }
        }
    }

    public function single($id) {
        $zip = Zip::where('id', $id)->with('editedZip')->with('client')->first();
        return Inertia::render('ZipUpload/SingleZip', [
            'zip' => $zip
        ]);
    }
}
